import IndexPage from "@/components/site/Index"
import About from "@/components/site/About"
import NotFound from "@/components/site/NotFound"

export default [{
    path: '/',
    name: 'IndexPage',
    component: IndexPage
  },
  {
    path: '/about',
    name: 'About',
    component: About
  }, {
    path: '/404',
    component: NotFound
  },
  {
    path: '*',
    redirect: '/404'
  },
]
