import Vue from "vue"
import Router from "vue-router"
import store from '../stores'

import User from "@/router/user"
import Admin from "@/router/admin"
import Dashboard from "@/router/dashboard"
import Page from "@/router/page"
import Site from "@/router/site"


Vue.use(Router)

let routes = new Router({
  mode: 'history',
  routes: [
    ...User,
    ...Admin,
    ...Dashboard,
    ...Dashboard,
    ...Page,
    ...Site // doit etre mis en dernier
  ]
});

/*
meta: { 
  requiresAuth: true,
  is_admin : true
  guest: true
  as_role: le_role_en_question
}

permlevel = {
  "guest": 0
}
*/
/*
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('jwt') == null) {
      next({
        path: '/login',
        params: {
          nextUrl: to.fullPath
        }
      })
    } else {
      let user = JSON.parse(localStorage.getItem('user'))
      if (to.matched.some(record => record.meta.is_admin)) {
        if (user.is_admin == 1) {
          next()
        } else {
          next({
            name: 'userboard'
          })
        }
      } else {
        next()
      }
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (localStorage.getItem('jwt') == null) {
      next()
    } else {
      next({
        name: 'userboard'
      })
    }
  } else {
    next()
  }
})*/

routes.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    console.log("isLoggedIn: " + store.getters.isLoggedIn);
    console.log("status: " + store.getters.authStatus);
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})


export default routes
