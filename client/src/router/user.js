import  Login from "@/components/user/Login"
import  Subscribe from "@/components/user/Subscribe"
export default [{
  path: '/login',
  name: 'Login',
  component: Login
}, {
  path: '/inscription',
  name: 'Subscribe',
  component: Subscribe
}]
