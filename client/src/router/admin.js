import Admin from "@/components/admin/Admin"
import ListPage from "@/components/page/List"
export default [{
  path: '/admin',
  component: Admin,
  meta: {
    layout: "admin",
    requiresAuth: true
  }
}, {
  path: '/admin/pages',
  name: 'ListPage',
  component: ListPage
},{
  path: '/admin/pages/modify/:id',
  name: 'modifPage',
  component: ListPage
}, {
  path: '/admin/settings',
  component: Admin,
  meta: {
    layout: "admin",
    requiresAuth: true
  }
}]
