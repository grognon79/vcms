const db = require('../src/db');

const NodeSchema = new db.Schema({
    name: { type: String, required: true },
    description: { type: String },
    model: { type: String, required: true }
});

const NodeModel = db.model('Node', NodeSchema);

module.exports = NodeModel;